package com.sunshare.sunshare;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.sunshare.fragments.AddBankAccount;
import com.sunshare.fragments.AddCreditCard;

/**
 * Created by adhithya on 20/7/16.
 */
public class AddCards extends FragmentActivity implements View.OnClickListener {

    ViewPager vpPager;
    ViewPagerAdapter viewPagerAdapter;
    SmartTabLayout viewPagerTab;

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView back;

    String toolBarTitleNames[] = {"Credit/Debit Card", "Banking Account"};

    private static final int NUM_PAGES = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cards);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolBarTitle);
        back = (ImageView) findViewById(R.id.back);

        toolbarTitle.setText(toolBarTitleNames[0]);
        toolbarTitle.setTextColor(Color.parseColor("#282828"));

        viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);

        vpPager = (ViewPager) findViewById(R.id.vpPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        vpPager.setAdapter(viewPagerAdapter);

        viewPagerTab.setViewPager(vpPager);

        vpPager.setClipToPadding(false);
        vpPager.setPageMargin(30);
        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                toolbarTitle.setText(toolBarTitleNames[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        vpPager.setHorizontalFadingEdgeEnabled(false);
//        vpPager.setFadingEdgeLength(80);

        Typeface tf = Typeface.createFromAsset(getAssets(),
                "GOTHAM-BOOK.OTF");
        toolbarTitle.setTypeface(tf);

        back.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
        }

    }


    public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {

                case 0:
                    return AddCreditCard.newInstance(position);
                case 1:
                    return AddBankAccount.newInstance(position);
                default:
                    return null;

            }

        }

    }

}
