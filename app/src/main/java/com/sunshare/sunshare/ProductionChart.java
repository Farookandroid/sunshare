package com.sunshare.sunshare;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.sunshare.fragments.ChartFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adhithya on 14/7/16.
 */
public class ProductionChart extends FragmentActivity {

    ViewPager vpPager;
    PagerAdapter mPagerAdapter;
    private static final int NUM_PAGES = 5;

    String[] vPagerNames = {"Today", "Week", "Month", "Year", "Lifetime"};

    TextView txtEnergyGuide, energy;
    Toolbar toolbar;
    TextView toolbarTitle;

    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_chart);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolBarTitle);
        vpPager = (ViewPager) findViewById(R.id.vpPager);
        txtEnergyGuide = (TextView) findViewById(R.id.txtEnergyGuide);
        energy = (TextView) findViewById(R.id.energy);
        back = (ImageView) findViewById(R.id.back);

        toolbarTitle.setText("Production");
        toolbarTitle.setTextColor(Color.parseColor("#282828"));

        Typeface tf = Typeface.createFromAsset(getAssets(),
                "GOTHAM-BOOK.OTF");
        Typeface tf2 = Typeface.createFromAsset(getAssets(),
                "GOTHAM-MEDIUM.OTF");

        txtEnergyGuide.setTypeface(tf2);
        energy.setTypeface(tf);

        vpPager.setPageMargin(150);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), vPagerNames);
        vpPager.setAdapter(mPagerAdapter);
        vpPager.setOffscreenPageLimit(0);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(vpPager);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm, String[] vPagerNames) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {

            return ChartFragment.newInstance(pos);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return vPagerNames[0];
                case 1:
                    return vPagerNames[1];
                case 2:
                    return vPagerNames[2];
                case 3:
                    return vPagerNames[3];
                case 4:
                    return vPagerNames[4];
                default:
                    break;
            }

            return super.getPageTitle(position);
        }

    }

}
