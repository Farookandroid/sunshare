package com.sunshare.sunshare;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import com.sunshare.ModelClasses.AddCardModel;
import com.sunshare.fragments.EnvironmentalImpact;
import com.sunshare.fragments.GardenInformation;
import com.sunshare.fragments.GardenProduction;
import com.sunshare.fragments.MyAccount;
import com.sunshare.fragments.PayMyBill;
import com.sunshare.fragments.PaymentSettings;
import com.sunshare.fragments.ReachUs;
import com.sunshare.fragments.ReferAFriend;
import com.sunshare.fragments.TransferService;
import com.sunshare.fragments.YourDocuments;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager mFragmentmanager;

    public static ArrayList<AddCardModel> addCardModelArrayList;
    AddCardModel addCardModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#000000"));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Garden Production");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
            }
        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mFragmentmanager = getSupportFragmentManager();

        GardenProduction mGardenproduction = new GardenProduction();
        mFragmentmanager.beginTransaction()
                .replace(R.id.Nav_container, mGardenproduction).commit();

        addCardModelArrayList = new ArrayList<AddCardModel>();
        addCardModel = new AddCardModel();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_garden_production) {
            // Handle the camera action

            GardenProduction mGardenProduction = new GardenProduction();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, mGardenProduction).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Garden Production");

        } else if (id == R.id.nav_garden_information) {

            GardenInformation mGardenInformation = new GardenInformation();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, mGardenInformation).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Garden Information");

        } else if (id == R.id.nav_my_account) {

            MyAccount myAccount = new MyAccount();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, myAccount).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("My Account");

        } else if (id == R.id.nav_pay_bill) {

            PayMyBill mPayMyBill = new PayMyBill();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, mPayMyBill).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Pay Bill");

        } else if (id == R.id.nav_environmental_impact) {

            EnvironmentalImpact mEnvironmentalImpact = new EnvironmentalImpact();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, mEnvironmentalImpact).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Environmental Impact");

        } else if (id == R.id.nav_payment_settings) {

            PaymentSettings paymentSettings = new PaymentSettings();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, paymentSettings).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Payment Settings");

        } else if (id == R.id.nav_transfer_service) {

            TransferService mTransferService = new TransferService();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, mTransferService).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Transfer Service");

        } else if (id == R.id.nav_refer_friend) {

            ReferAFriend mReferAFriend = new ReferAFriend();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, mReferAFriend).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Refer A Friend");


        } else if (id == R.id.nav_your_documents) {

            YourDocuments mYourDocuments = new YourDocuments();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, mYourDocuments).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Your Documents");

        } else if (id == R.id.nav_reach_us) {
            ReachUs reachUs = new ReachUs();
            mFragmentmanager.beginTransaction()
                    .replace(R.id.Nav_container, reachUs).commit();
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Reach Us");

        } else if (id == R.id.nav_logout) {

            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
