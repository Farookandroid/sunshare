package com.sunshare.sunshare;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by user on 13/7/16.
 */
public class Login extends Activity implements View.OnClickListener {

    TextView userName, password;
    Button login;
    TextView forgotPassword, register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

//        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().setStatusBarColor(Color.TRANSPARENT);
//            setStatusBarTranslucent(true);
//        }

        setContentView(R.layout.activity_login);


        userName = (TextView) findViewById(R.id.userName);
        password = (TextView) findViewById(R.id.password);
        forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        register = (TextView) findViewById(R.id.register);
        login = (Button) findViewById(R.id.login);

        Typeface tf = Typeface.createFromAsset(getAssets(),
                "GOTHAM-BOOK.OTF");
        Typeface tf2 = Typeface.createFromAsset(getAssets(),
                "GOTHAM-MEDIUM.OTF");

        userName.setTypeface(tf);
        password.setTypeface(tf);
        forgotPassword.setTypeface(tf2);
        register.setTypeface(tf2);
        login.setTypeface(tf2);

        register.setVisibility(View.GONE);

        login.setOnClickListener(this);

        userName.setCursorVisible(false);
        password.setCursorVisible(false);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.login:

                Intent intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);
        }

    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
