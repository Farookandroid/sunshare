package com.sunshare.sunshare;

import android.app.Application;

import com.sunshare.Utils.FontsOverride;

/**
 * Created by Adhithya on 21/7/16.
 */
public class SunShareApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FontsOverride.setDefaultFont(getApplicationContext(), "SERIF",
                "GOTHAM-BOOK.OTF");


    }


}
