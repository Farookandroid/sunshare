package com.sunshare.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sunshare.ModelClasses.AddCardModel;
import com.sunshare.sunshare.AddCards;
import com.sunshare.sunshare.MainActivity;
import com.sunshare.sunshare.R;

import java.util.ArrayList;

/**
 * Created by farook on 13/7/16.
 */
public class PayMyBill extends Fragment implements View.OnClickListener {

    TextView txt_bill, txt_amount, txt_billing_cycle, txt_due_date, txt_due_date_txt, txt_previous, txt_previous_txt, txt_garden, garden_production, txt_donation_edit, txt_ssa, ssa, txt_donation, txt_donation_txt, txt_charge, charge;
    Button btn_pay_now;
    EditText edt_donation;

    ViewPager vpPager;
    ViewPagerAdapter viewPagerAdapter;

    AddCardModel model;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pay_my_bill, container, false);

        initializeUI(view);

        model = new AddCardModel();

        vpPager = (ViewPager) view.findViewById(R.id.vpPager);
        viewPagerAdapter = new ViewPagerAdapter(getActivity(), MainActivity.addCardModelArrayList);
        vpPager.setAdapter(viewPagerAdapter);
        vpPager.setClipToPadding(false);
        vpPager.setPageMargin(30);
        vpPager.setHorizontalFadingEdgeEnabled(false);
        vpPager.setFadingEdgeLength(80);

        vpPager.setVisibility(View.GONE);

        return view;
    }

    private void initializeUI(View view) {

        txt_bill = (TextView) view.findViewById(R.id.txt_bill_amount);
        txt_amount = (TextView) view.findViewById(R.id.bill_amount);
        txt_due_date = (TextView) view.findViewById(R.id.txt_due_date);
        txt_due_date_txt = (TextView) view.findViewById(R.id.due_date);
        txt_previous = (TextView) view.findViewById(R.id.txt_previous_payment);
        txt_previous_txt = (TextView) view.findViewById(R.id.previous_payment);

        txt_billing_cycle = (TextView) view.findViewById(R.id.billing);

        txt_garden = (TextView) view.findViewById(R.id.txt_garden_production);
        txt_ssa = (TextView) view.findViewById(R.id.txt_ssa);
        txt_donation_edit = (TextView) view.findViewById(R.id.txt_donation_edit);
        txt_donation_txt = (TextView) view.findViewById(R.id.txt_donation);
        txt_charge = (TextView) view.findViewById(R.id.txt_charge);

        garden_production = (TextView) view.findViewById(R.id.garden_production);
        ssa = (TextView) view.findViewById(R.id.ssa);
        charge = (TextView) view.findViewById(R.id.charge);

        edt_donation = (EditText) view.findViewById(R.id.edt_donation);
        btn_pay_now = (Button) view.findViewById(R.id.pay_now);

        Typeface fonttxtb = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOLD.OTF");
        Typeface fonttxt = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOOK.OTF");
        Typeface fontmedium = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-MEDIUM.OTF");


        txt_bill.setTypeface(fonttxtb);
        txt_amount.setTypeface(fonttxt);
        txt_due_date.setTypeface(fonttxt);
        txt_due_date_txt.setTypeface(fonttxt);
        txt_previous.setTypeface(fonttxt);
        txt_previous_txt.setTypeface(fonttxt);

        txt_billing_cycle.setTypeface(fonttxtb);

        txt_garden.setTypeface(fontmedium);
        garden_production.setTypeface(fonttxt);
        txt_ssa.setTypeface(fontmedium);
        ssa.setTypeface(fonttxt);
        edt_donation.setTypeface(fontmedium);
        txt_donation_edit.setTypeface(fonttxt);
        txt_donation_txt.setTypeface(fonttxt);
        txt_charge.setTypeface(fontmedium);
        charge.setTypeface(fonttxt);

        btn_pay_now.setTypeface(fonttxt);

        edt_donation.setCursorVisible(false);

        btn_pay_now.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pay_now:

                vpPager.setVisibility(View.VISIBLE);

                if (MainActivity.addCardModelArrayList.size() == 0) {
                    Toast.makeText(getActivity(), "Please save your card details", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), AddCards.class);
                    startActivityForResult(intent, 2);
                }

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2 && data != null) {
            Bundle bundle = data.getExtras();
            model = (AddCardModel) bundle.getSerializable("value");
            MainActivity.addCardModelArrayList.add(model);
            viewPagerAdapter.notifyDataSetChanged();
        }
    }


    public class ViewPagerAdapter extends PagerAdapter {

        Context context;
        LayoutInflater inflater;

        ImageView img_select_card, img_card_type;
        TextView txt_your_account_number, your_account_number, expires_on;
        LinearLayout card_layout;

        AddCardModel model;
        ArrayList<AddCardModel> addCardModels;

        public ViewPagerAdapter(Context context, ArrayList<AddCardModel> addCardModels) {
            this.context = context;
            this.addCardModels = addCardModels;

        }

        @Override
        public int getCount() {
//            return impactBg.length;
            return addCardModels.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.layout_cards,
                    container, false);

            if (addCardModels != null) {

                txt_your_account_number = (TextView) itemView.findViewById(R.id.txt_your_account_number);
                your_account_number = (TextView) itemView.findViewById(R.id.your_account_number);
                expires_on = (TextView) itemView.findViewById(R.id.expires_on);

                card_layout = (LinearLayout) itemView.findViewById(R.id.card_layout);

                img_select_card = (ImageView) itemView.findViewById(R.id.img_select_card);
                img_card_type = (ImageView) itemView.findViewById(R.id.img_card_type);

                Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                        "GOTHAM-LIGHT.OTF");

                txt_your_account_number.setTypeface(tf);
                your_account_number.setTypeface(tf);
                expires_on.setTypeface(tf);


                if (addCardModels.get(position).getCardType().equalsIgnoreCase("Debit Card")) {

//                    card_layout.setBackgroundResource(R.drawable.visa_card_bg);
                    img_card_type.setImageResource(R.drawable.ic_visa);
                    your_account_number.setText(addCardModels.get(position).getCardNumber());
                    expires_on.setText("Expires on: " + addCardModels.get(position).getCardMonth() + "/" + addCardModels.get(position).getCardYear());

                } else if (addCardModels.get(position).getCardType().equalsIgnoreCase("Bank Account")) {

//                    card_layout.setBackgroundResource(R.drawable.banking_account_bg);
                    img_card_type.setImageResource(R.drawable.bank_account);
                    your_account_number.setText(addCardModels.get(position).getAccountNumber());
                    expires_on.setText("Routing Number: " + addCardModels.get(position).getRoutingNumber());
                }


            }


            ((ViewPager) container).addView(itemView);


            return itemView;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((LinearLayout) object);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

}
