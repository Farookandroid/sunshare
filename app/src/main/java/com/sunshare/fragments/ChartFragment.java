package com.sunshare.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.sunshare.sunshare.R;

import java.util.ArrayList;

/**
 * Created by user on 14/7/16.
 */
public class ChartFragment extends Fragment {

    private CombinedChart mChart;
    private final int itemcount = 12;

    private String[] mMonths = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chart, container, false);


        mChart = (CombinedChart) view.findViewById(R.id.chart1);
        mChart.setDescription("");
        mChart.setBackgroundColor(Color.WHITE);
        mChart.setDrawGridBackground(false);
        mChart.setDrawBarShadow(false);
        mChart.setHighlightFullBarEnabled(false);
        mChart.setTouchEnabled(false);
        mChart.setPinchZoom(false);
        mChart.animateXY(1000, 1000);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinValue(0f);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new AxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return mMonths[(int) value % mMonths.length];
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });


        CombinedData data = new CombinedData();
        data.setData(barData());
        data.setData(lineData());
        mChart.setData(data);


        return view;

    }

    public LineData lineData() {
        ArrayList<Entry> line = new ArrayList<>();
        line.add(new Entry(0, 2f));
        line.add(new Entry(1, 3f));
        line.add(new Entry(2, 5f));
        line.add(new Entry(3, 6f));
        line.add(new Entry(4, 8f));
        line.add(new Entry(5, 4f));
        line.add(new Entry(6, 6f));
        line.add(new Entry(7, 8f));
        line.add(new Entry(8, 5f));
        line.add(new Entry(9, 7f));
        line.add(new Entry(10, 4f));
        line.add(new Entry(11, 8f));

        LineDataSet lineDataSet = new LineDataSet(line, "Brand 2");
        lineDataSet.setColor(Color.parseColor("#FF722C"));
        lineDataSet.setCircleColor(Color.parseColor("#FF722C"));
        lineDataSet.setValueTextColor(Color.parseColor("#FF722C"));

//        lineDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        LineData lineData = new LineData(lineDataSet);

        return lineData;

    }

    // this method is used to create data for Bar graph
    public BarData barData() {

        ArrayList<BarEntry> group1 = new ArrayList<>();
        group1.add(new BarEntry(0, 4f));
        group1.add(new BarEntry(1, 5f));
        group1.add(new BarEntry(2, 3f));
        group1.add(new BarEntry(3, 7f));
        group1.add(new BarEntry(4, 6f));
        group1.add(new BarEntry(5, 2f));
        group1.add(new BarEntry(6, 6f));
        group1.add(new BarEntry(7, 5f));
        group1.add(new BarEntry(8, 8f));
        group1.add(new BarEntry(9, 6f));
        group1.add(new BarEntry(10, 3f));
        group1.add(new BarEntry(11, 5f));


        BarDataSet barDataSet = new BarDataSet(group1, "Brand 1");
        barDataSet.setColor(Color.parseColor("#003B64"));
        barDataSet.setValueTextColor(Color.parseColor("#003B64"));
        barDataSet.setBarBorderWidth(2f);
        barDataSet.setBarBorderColor(Color.parseColor("#ffffff"));

//        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        BarData barData = new BarData(barDataSet);
        return barData;

    }


    public static Fragment newInstance(int position) {

        ChartFragment chartFragment = new ChartFragment();
        return chartFragment;

    }


}