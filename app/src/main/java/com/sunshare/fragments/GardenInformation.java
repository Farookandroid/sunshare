package com.sunshare.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunshare.sunshare.R;

/**
 * Created by user on 18/7/16.
 */
public class GardenInformation extends Fragment {

    TextView txt_garden_name, txt_garden_address, txt_subscriber_complaints, txt_county, txt_craver;
    TextView garden_name, garden_address, subscriber_complaints, county, craver;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_garden_information, container, false);

        initializeUI(view);

        return view;
    }

    private void initializeUI(View view) {

        txt_garden_name = (TextView) view.findViewById(R.id.txt_garden_name);
        txt_garden_address = (TextView) view.findViewById(R.id.txt_garden_address);
        txt_subscriber_complaints = (TextView) view.findViewById(R.id.txt_subscriber_complaints);
        txt_county = (TextView) view.findViewById(R.id.txt_county);
        txt_craver = (TextView) view.findViewById(R.id.txt_craver);

        garden_name = (TextView) view.findViewById(R.id.garden_name);
        garden_address = (TextView) view.findViewById(R.id.garden_address);
        subscriber_complaints = (TextView) view.findViewById(R.id.subscriber_complaints);
        county = (TextView) view.findViewById(R.id.county);
        craver = (TextView) view.findViewById(R.id.craver);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOOK.OTF");
        Typeface fontbold = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOLD.OTF");

        txt_garden_name.setTypeface(typeface);
        txt_garden_address.setTypeface(typeface);
        txt_subscriber_complaints.setTypeface(typeface);
        txt_county.setTypeface(typeface);
        txt_craver.setTypeface(fontbold);

        garden_name.setTypeface(fontbold);
        garden_address.setTypeface(fontbold);
        subscriber_complaints.setTypeface(fontbold);
        county.setTypeface(typeface);
        craver.setTypeface(fontbold);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

}
