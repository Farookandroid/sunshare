package com.sunshare.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sunshare.sunshare.R;

/**
 * Created by user on 19/7/16.
 */
public class MyAccount extends Fragment {

    TextView txt_customer_name, customer_name, txt_userid, user_id, txt_phone_number, txt_altnum;
    TextView txt_account_number, account_number;
    EditText phone_number, alt_num;
    Button btn_change_phone;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        initializeUI(view);

        return view;
    }

    private void initializeUI(View view) {

        txt_customer_name = (TextView) view.findViewById(R.id.txt_customer_name);
        customer_name = (TextView) view.findViewById(R.id.customer_name);
        txt_userid = (TextView) view.findViewById(R.id.txt_userid);
        user_id = (TextView) view.findViewById(R.id.user_id);
        txt_phone_number = (TextView) view.findViewById(R.id.txt_phone_number);
        txt_altnum = (TextView) view.findViewById(R.id.txt_altnum);

        txt_account_number = (TextView) view.findViewById(R.id.txt_account_number);
        account_number = (TextView) view.findViewById(R.id.account_number);

        phone_number = (EditText) view.findViewById(R.id.phone_number);
        alt_num = (EditText) view.findViewById(R.id.alt_num);

        btn_change_phone = (Button) view.findViewById(R.id.btn_change_phone);

        Typeface fontbold = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOLD.OTF");
        Typeface fonttxt = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOOK.OTF");
        Typeface fontmedium = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-MEDIUM.OTF");

        account_number.setTypeface(fontbold);
        txt_account_number.setTypeface(fonttxt);

        txt_customer_name.setTypeface(fonttxt);
        customer_name.setTypeface(fontbold);
        txt_userid.setTypeface(fonttxt);
        user_id.setTypeface(fontbold);
        txt_phone_number.setTypeface(fonttxt);
        txt_altnum.setTypeface(fonttxt);

        phone_number.setTypeface(fontbold);
        alt_num.setTypeface(fontbold);

        btn_change_phone.setTypeface(fontmedium);

        phone_number.setCursorVisible(false);
        alt_num.setCursorVisible(false);

    }
}