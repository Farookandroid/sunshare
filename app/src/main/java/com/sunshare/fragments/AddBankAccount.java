package com.sunshare.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sunshare.ModelClasses.AddCardModel;
import com.sunshare.sunshare.R;

import java.util.ArrayList;

/**
 * Created by user on 20/7/16.
 */
public class AddBankAccount extends Fragment implements View.OnClickListener {

    TextView txt_first_name, txt_last_name, txt_routing_number, txt_account_number;
    EditText first_name, last_name, routing_number, account_number;
    Button linkAccount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_bank_account, container, false);

        intializeUI(view);

        linkAccount.setOnClickListener(this);

        return view;
    }


    public static Fragment newInstance(int position) {

        AddBankAccount addBankAccount = new AddBankAccount();
        return addBankAccount;
    }

    private void intializeUI(View view) {
        txt_first_name = (TextView) view.findViewById(R.id.txt_first_name);
        txt_last_name = (TextView) view.findViewById(R.id.txt_last_name);
        txt_routing_number = (TextView) view.findViewById(R.id.txt_routing_number);
        txt_account_number = (TextView) view.findViewById(R.id.txt_account_number);

        first_name = (EditText) view.findViewById(R.id.first_name);
        last_name = (EditText) view.findViewById(R.id.last_name);
        routing_number = (EditText) view.findViewById(R.id.routing_number);
        account_number = (EditText) view.findViewById(R.id.account_number);

        linkAccount = (Button) view.findViewById(R.id.link_account);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                "GOTHAM-LIGHT.OTF");

        txt_first_name.setTypeface(tf);
        txt_last_name.setTypeface(tf);
        txt_routing_number.setTypeface(tf);
        txt_account_number.setTypeface(tf);

        first_name.setTypeface(tf);
        last_name.setTypeface(tf);
        routing_number.setTypeface(tf);
        account_number.setTypeface(tf);

        linkAccount.setTypeface(tf);


        first_name.setCursorVisible(false);
        last_name.setCursorVisible(false);
        routing_number.setCursorVisible(false);
        account_number.setCursorVisible(false);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.first_name:
                first_name.setCursorVisible(true);
                break;
            case R.id.last_name:
                last_name.setCursorVisible(true);
                break;
            case R.id.routing_number:
                routing_number.setCursorVisible(true);
                break;
            case R.id.account_number:
                account_number.setCursorVisible(true);
                break;


            case R.id.link_account:

                // cardModels = new ArrayList<AddCardModel>();

                AddCardModel model = new AddCardModel();

                model.setCardType("Bank Account");
                model.setFirstName(first_name.getText().toString());
                model.setLastName(last_name.getText().toString());
                model.setRoutingNumber(routing_number.getText().toString());
                model.setAccountNumber(account_number.getText().toString());

                //  cardModels.add(model);

                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("value", model);
                intent.putExtras(bundle);
                getActivity().setResult(2, intent);
                getActivity().finish();


                break;
            default:
                break;
        }

    }
}
