package com.sunshare.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunshare.ModelClasses.AddCardModel;
import com.sunshare.sunshare.MainActivity;
import com.sunshare.sunshare.R;

import java.util.ArrayList;

/**
 * Created by user on 23/7/16.
 */
public class CardLayoutFragment extends Fragment {

    public static int pos;

    ImageView img_select_card, img_card_type;
    TextView txt_your_account_number, your_account_number, expires_on;
    LinearLayout card_layout;

    AddCardModel model;

    Boolean isChecked = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_cards, container, false);

        intializeUI(view);


        if (MainActivity.addCardModelArrayList.get(pos).getCardType().equalsIgnoreCase("Debit Card")) {

//                    card_layout.setBackgroundResource(R.drawable.visa_card_bg);
            img_card_type.setImageResource(R.drawable.ic_visa);
            your_account_number.setText(MainActivity.addCardModelArrayList.get(pos).getCardNumber());
            expires_on.setText("Expires on: " + MainActivity.addCardModelArrayList.get(pos).getCardMonth() + "/" + MainActivity.addCardModelArrayList.get(pos).getCardYear());

        } else if (MainActivity.addCardModelArrayList.get(pos).getCardType().equalsIgnoreCase("Bank Account")) {

//                    card_layout.setBackgroundResource(R.drawable.banking_account_bg);
            img_card_type.setImageResource(R.drawable.bank_account);
            your_account_number.setText(MainActivity.addCardModelArrayList.get(pos).getAccountNumber());
            expires_on.setText("Routing Number: " + MainActivity.addCardModelArrayList.get(pos).getRoutingNumber());
        }


        img_select_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isChecked) {
                    img_select_card.setImageResource(R.drawable.cheked);
                    isChecked = true;
                } else {
                    img_select_card.setImageResource(R.drawable.unchecked);
                    isChecked = false;
                }

            }
        });


        return view;
    }

    private void intializeUI(View view) {

        txt_your_account_number = (TextView) view.findViewById(R.id.txt_your_account_number);
        your_account_number = (TextView) view.findViewById(R.id.your_account_number);
        expires_on = (TextView) view.findViewById(R.id.expires_on);

        card_layout = (LinearLayout) view.findViewById(R.id.card_layout);

        img_select_card = (ImageView) view.findViewById(R.id.img_select_card);
        img_card_type = (ImageView) view.findViewById(R.id.img_card_type);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                "GOTHAM-LIGHT.OTF");

        txt_your_account_number.setTypeface(tf);
        your_account_number.setTypeface(tf);
        expires_on.setTypeface(tf);

        img_select_card.setImageResource(R.drawable.unchecked);


    }

    public static Fragment newInstance(int position) {

        pos = position;

        CardLayoutFragment cardLayoutFragment = new CardLayoutFragment();
        return cardLayoutFragment;
    }

}
