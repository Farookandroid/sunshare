package com.sunshare.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sunshare.Adapters.PdfListAdapter;
import com.sunshare.sunshare.R;

/**
 * Created by user on 13/7/16.
 */
public class YourDocuments extends Fragment {

    ListView pdfListView;

    public static String[] prgmNameList = {"Customer Consent Form.pdf", "Community Solar Srvices Agreement.pdf", "Subscriber Agency Agreement Form.pdf"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_yourdocument, container, false);

        pdfListView = (ListView) view.findViewById(R.id.listView);
        pdfListView.setAdapter(new PdfListAdapter(getActivity(), prgmNameList));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

}
