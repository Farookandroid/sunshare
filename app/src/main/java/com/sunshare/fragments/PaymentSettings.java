package com.sunshare.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunshare.ModelClasses.AddCardModel;
import com.sunshare.sunshare.AddCards;
import com.sunshare.sunshare.MainActivity;
import com.sunshare.sunshare.R;

import java.util.ArrayList;

/**
 * Created by user on 20/7/16.
 */
public class PaymentSettings extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    public SwitchCompat paperless_bill_switch, auto_pay_switch;
    TextView txt_your_cards;

    ViewPager vpPager;
    //    ViewPagerAdapter viewPagerAdapter;
    FragmentViewPagerAdapter adapter;

    int checked_position = -1;

    //    ArrayList<AddCardModel> addCardModelArrayList;
    AddCardModel model;

    LinearLayout delete_card_layout;
    ImageView img_delete_ok, img_delete_cancel;
    TextView txt_delete_card;

    ImageView img_delete_card, img_no_card;

    int cardPosition = -1;
    Boolean isChecked = false;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(true);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_settings, container, false);
        initializeUI(view);

//        addCardModelArrayList = new ArrayList<AddCardModel>();
        model = new AddCardModel();

        adapter = new FragmentViewPagerAdapter(getFragmentManager());

//        viewPagerAdapter = new ViewPagerAdapter(getActivity(), MainActivity.addCardModelArrayList);
//        vpPager.setOffscreenPageLimit(3);
//        vpPager.setAdapter(viewPagerAdapter);

        vpPager.setAdapter(adapter);

        vpPager.setClipToPadding(false);
        vpPager.setPageMargin(30);
        vpPager.setHorizontalFadingEdgeEnabled(false);
        vpPager.setFadingEdgeLength(80);
        vpPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        paperless_bill_switch.setOnCheckedChangeListener(this);
        auto_pay_switch.setOnCheckedChangeListener(this);

        return view;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.add_card:

                addCard(getActivity());

                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2 && data != null) {
            Bundle bundle = data.getExtras();
            model = (AddCardModel) bundle.getSerializable("value");
            MainActivity.addCardModelArrayList.add(model);
//            viewPagerAdapter.notifyDataSetChanged();

            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        vpPager.setOffscreenPageLimit(adapter.getCount());

//        viewPagerAdapter.notifyDataSetChanged();

        adapter.notifyDataSetChanged();

        if (MainActivity.addCardModelArrayList != null && MainActivity.addCardModelArrayList.size() == 0) {
            img_delete_card.setVisibility(View.GONE);
            img_no_card.setVisibility(View.VISIBLE);
        } else {
            img_delete_card.setVisibility(View.VISIBLE);
            img_no_card.setVisibility(View.GONE);
        }

    }

    private void initializeUI(View view) {

        paperless_bill_switch = (SwitchCompat) view.findViewById(R.id.paperless_bill_switch);
        auto_pay_switch = (SwitchCompat) view.findViewById(R.id.auto_pay_switch);

        txt_your_cards = (TextView) view.findViewById(R.id.txt_your_cards);
        txt_delete_card = (TextView) view.findViewById(R.id.txt_delete_card);

        delete_card_layout = (LinearLayout) view.findViewById(R.id.delete_card_layout);
        delete_card_layout.setVisibility(View.GONE);

        img_delete_ok = (ImageView) view.findViewById(R.id.img_delete_ok);
        img_delete_cancel = (ImageView) view.findViewById(R.id.img_delete_cancel);
        img_delete_card = (ImageView) view.findViewById(R.id.img_delete_card);

        img_no_card = (ImageView) view.findViewById(R.id.img_no_cards);

        Typeface fontbold = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOLD.OTF");
        Typeface fonttxt = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-BOOK.OTF");
        Typeface fontmedium = Typeface.createFromAsset(getActivity().getAssets(), "GOTHAM-MEDIUM.OTF");

        paperless_bill_switch.setTypeface(fonttxt);
        auto_pay_switch.setTypeface(fonttxt);
        txt_your_cards.setTypeface(fonttxt);
        txt_delete_card.setTypeface(fontmedium);

        vpPager = (ViewPager) view.findViewById(R.id.vpPager);

        img_delete_ok.setOnClickListener(this);
        img_delete_cancel.setOnClickListener(this);
        img_delete_card.setOnClickListener(this);

        img_no_card.setOnClickListener(this);


    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        switch (compoundButton.getId()) {

            case R.id.paperless_bill_switch:

                if (isChecked) {

                    auto_pay_switch.setChecked(false);

                } else {


                }
                break;

            case R.id.auto_pay_switch:

                if (isChecked) {
                    paperless_bill_switch.setChecked(false);


                    if (MainActivity.addCardModelArrayList != null && MainActivity.addCardModelArrayList.size() == 0) {
                        Intent intent = new Intent(getActivity(), AddCards.class);
                        startActivityForResult(intent, 2);
                    }

                } else {


                }
                break;


            default:
                break;
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_no_cards:
                if (MainActivity.addCardModelArrayList != null && MainActivity.addCardModelArrayList.size() == 0) {
                    Intent intent = new Intent(getActivity(), AddCards.class);
                    startActivityForResult(intent, 2);
                }
                break;

            case R.id.img_delete_card:

                delete_card_layout.setVisibility(View.VISIBLE);
                txt_delete_card.setVisibility(View.VISIBLE);
                img_delete_cancel.setVisibility(View.VISIBLE);
                img_delete_ok.setVisibility(View.VISIBLE);

                break;
            case R.id.img_delete_cancel:

                delete_card_layout.setVisibility(View.GONE);
                txt_delete_card.setVisibility(View.GONE);
                img_delete_cancel.setVisibility(View.GONE);
                img_delete_ok.setVisibility(View.GONE);

                break;
            case R.id.img_delete_ok:

                delete_card_layout.setVisibility(View.GONE);
                txt_delete_card.setVisibility(View.GONE);
                img_delete_cancel.setVisibility(View.GONE);
                img_delete_ok.setVisibility(View.GONE);

                MainActivity.addCardModelArrayList.remove(vpPager.getCurrentItem());
                Log.v("CurrentItem", "" + vpPager.getCurrentItem());
//              //  viewPagerAdapter.notifyDataSetChanged();

//                vpPager.setAdapter(viewPagerAdapter);

                vpPager.setAdapter(adapter);

                onResume();

                break;
        }

    }

//    public class ViewPagerAdapter extends PagerAdapter {
//
//        Context context;
//        LayoutInflater inflater;
//
//        ImageView img_select_card, img_card_type;
//        TextView txt_your_account_number, your_account_number, expires_on;
//        LinearLayout card_layout;
//
//        AddCardModel model;
//        ArrayList<AddCardModel> addCardModels;
//
//        public ViewPagerAdapter(Context context, ArrayList<AddCardModel> addCardModels) {
//            this.context = context;
//            this.addCardModels = addCardModels;
//
//        }
//
//        @Override
//        public int getCount() {
////            return impactBg.length;
//            Log.v("PaymentSet", "" + addCardModels.size());
//            return addCardModels.size();
//
//        }
//
//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            return view == ((LinearLayout) object);
//        }
//
//        @Override
//        public void notifyDataSetChanged() {
//            super.notifyDataSetChanged();
//        }
//
//        @Override
//        public Object instantiateItem(ViewGroup container, final int position) {
//
//            inflater = (LayoutInflater) context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View itemView = inflater.inflate(R.layout.layout_cards,
//                    container, false);
//
//            if (addCardModels != null) {
//
//                img_delete_card.setVisibility(View.VISIBLE);
//                img_no_card.setVisibility(View.GONE);
//
//                txt_your_account_number = (TextView) itemView.findViewById(R.id.txt_your_account_number);
//                your_account_number = (TextView) itemView.findViewById(R.id.your_account_number);
//                expires_on = (TextView) itemView.findViewById(R.id.expires_on);
//
//                card_layout = (LinearLayout) itemView.findViewById(R.id.card_layout);
//
//                img_select_card = (ImageView) itemView.findViewById(R.id.img_select_card);
//                img_card_type = (ImageView) itemView.findViewById(R.id.img_card_type);
//
//                Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
//                        "GOTHAM-LIGHT.OTF");
//
//                txt_your_account_number.setTypeface(tf);
//                your_account_number.setTypeface(tf);
//                expires_on.setTypeface(tf);
//
//                img_select_card.setImageResource(R.drawable.unchecked);
//
////                if (checked_position != -1 && checked_position == position) {
////                    img_select_card.setImageResource(R.drawable.cheked);
////                } else {
////
////                    img_select_card.setImageResource(R.drawable.unchecked);
////                }
//
//
//                if (addCardModels.get(position).getCardType().equalsIgnoreCase("Debit Card")) {
//
////                    card_layout.setBackgroundResource(R.drawable.visa_card_bg);
//                    img_card_type.setImageResource(R.drawable.ic_visa);
//                    your_account_number.setText(addCardModels.get(position).getCardNumber());
//                    expires_on.setText("Expires on: " + addCardModels.get(position).getCardMonth() + "/" + addCardModels.get(position).getCardYear());
//
//                } else if (addCardModels.get(position).getCardType().equalsIgnoreCase("Bank Account")) {
//
////                    card_layout.setBackgroundResource(R.drawable.banking_account_bg);
//                    img_card_type.setImageResource(R.drawable.bank_account);
//                    your_account_number.setText(addCardModels.get(position).getAccountNumber());
//                    expires_on.setText("Routing Number: " + addCardModels.get(position).getRoutingNumber());
//                }
//
//
//                img_select_card.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        if (!isChecked) {
//                            img_select_card.setImageResource(R.drawable.cheked);
//                            isChecked = true;
//                        } else {
//                            img_select_card.setImageResource(R.drawable.unchecked);
//                            isChecked = false;
//                        }
//
//                        onResume();
//
//                    }
//                });
//
//
//            } else if (addCardModels.size() == 0) {
//                img_no_card.setVisibility(View.VISIBLE);
//                img_delete_card.setVisibility(View.GONE);
//            }
//
//
//            ((ViewPager) container).addView(itemView);
//
//
//            return itemView;
//        }
//
//
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object) {
//            ((ViewPager) container).removeView((LinearLayout) object);
//
//        }
//    }


    public void addCard(Activity activity) {
        Intent intent = new Intent(activity, AddCards.class);
        startActivityForResult(intent, 2);

    }


    // TODO: 23/7/16  Fragment Adapter created & pager adapter removed


    public class FragmentViewPagerAdapter extends FragmentStatePagerAdapter {

        public FragmentViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return MainActivity.addCardModelArrayList.size();
        }

        @Override
        public Fragment getItem(int position) {

            return CardLayoutFragment.newInstance(position);

        }

    }


}