package com.sunshare.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sunshare.sunshare.R;

/**
 * Created by user on 13/7/16.
 */
public class ReachUs extends Fragment {
    private GoogleMap googleMap;

    TextView txt_ph, txt_email, txt_website;
    String es;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reachus, container, false);

//        googleMap = ((SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.map)).getMap();


        // Add a marker at San Francisco.
//        Marker marker = googleMap.addMarker(new MarkerOptions()
//                .position(new LatLng(13.055079, 80.255791))
//                .title("Sunshare"));

        //  googleMap.addMarker(new MarkerOptions().position(13.055079,80.255791)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location)));


        txt_ph = (TextView) view.findViewById(R.id.edt_telephone);
        txt_email = (TextView) view.findViewById(R.id.edt_email);
        txt_website = (TextView) view.findViewById(R.id.edt_website);


        txt_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });


        txt_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("https://www.mysunshare.com/"));
                startActivity(browserIntent);
            }
        });


        txt_ph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                es = txt_ph.getText().toString();
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + es));
                startActivity(callIntent);
            }
        });

        return view;
    }

    protected void sendEmail() {
        String[] TO = {"info@mysunshare.com"};
        Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
        email.setType("message/rfc822");
        email.putExtra(Intent.EXTRA_EMAIL, TO);
        email.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
        email.putExtra(Intent.EXTRA_SUBJECT, "Reg- SunShare");
        try {
            startActivity(Intent.createChooser(email, "Choose an email client from"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "No email client installed.", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

}
