package com.sunshare.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunshare.sunshare.ProductionChart;
import com.sunshare.sunshare.R;

/**
 * Created by adhithya on 13/7/16.
 */
public class GardenProduction extends Fragment implements View.OnClickListener {

    TextView txtThisMonth, txtThisYear, txtLifeTime, txtSystemSize, txtBillForeCast, txtLastBill;
    TextView thisMonthPower, thisYearPower, lifeTimePower, systemSizePower, foreCastBill;
    TextView txtDay1Date, txtDay2Date, txtDay3Date, txtDay4Date, txtDay5Date;
    TextView Day1Temp, Day2Temp, Day3Temp, Day4Temp, Day5Temp;
    TextView Day1ProfitCost, Day2ProfitCost, Day3ProfitCost, Day4ProfitCost, Day5ProfitCost;
    ImageView imgDay1weather, imgDay2weather, imgDay3weather, imgDay4weather, imgDay5weather;

    LinearLayout thisMonthLayout, thisYearLayout, lifeTimeLayout, systemSizeLayout;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_garden_production, container, false);
        initializeUI(view);

        thisMonthLayout.setOnClickListener(this);
        thisYearLayout.setOnClickListener(this);
        lifeTimeLayout.setOnClickListener(this);
        systemSizeLayout.setOnClickListener(this);

        return view;
    }

    public void initializeUI(View view) {

        txtThisMonth = (TextView) view.findViewById(R.id.txtThisMonth);
        txtThisYear = (TextView) view.findViewById(R.id.txtThisYear);
        txtLifeTime = (TextView) view.findViewById(R.id.txtLifeTime);
        txtSystemSize = (TextView) view.findViewById(R.id.txtSystemSize);
        txtBillForeCast = (TextView) view.findViewById(R.id.txtBillForeCast);
        txtLastBill = (TextView) view.findViewById(R.id.txtLastBill);

        thisMonthPower = (TextView) view.findViewById(R.id.thisMonthPower);
        thisYearPower = (TextView) view.findViewById(R.id.thisYearPower);
        lifeTimePower = (TextView) view.findViewById(R.id.lifeTimePower);
        systemSizePower = (TextView) view.findViewById(R.id.systemSizePower);
        foreCastBill = (TextView) view.findViewById(R.id.foreCastBill);

        txtDay1Date = (TextView) view.findViewById(R.id.txtDay1Date);
        txtDay2Date = (TextView) view.findViewById(R.id.txtDay2Date);
        txtDay3Date = (TextView) view.findViewById(R.id.txtDay3Date);
        txtDay4Date = (TextView) view.findViewById(R.id.txtDay4Date);
        txtDay5Date = (TextView) view.findViewById(R.id.txtDay5Date);

        Day1Temp = (TextView) view.findViewById(R.id.Day1Temp);
        Day2Temp = (TextView) view.findViewById(R.id.Day2Temp);
        Day3Temp = (TextView) view.findViewById(R.id.Day3Temp);
        Day4Temp = (TextView) view.findViewById(R.id.Day4Temp);
        Day5Temp = (TextView) view.findViewById(R.id.Day5Temp);

        Day1ProfitCost = (TextView) view.findViewById(R.id.Day1ProfitCost);
        Day2ProfitCost = (TextView) view.findViewById(R.id.Day2ProfitCost);
        Day3ProfitCost = (TextView) view.findViewById(R.id.Day3ProfitCost);
        Day4ProfitCost = (TextView) view.findViewById(R.id.Day4ProfitCost);
        Day5ProfitCost = (TextView) view.findViewById(R.id.Day5ProfitCost);

        imgDay1weather = (ImageView) view.findViewById(R.id.imgDay1weather);
        imgDay2weather = (ImageView) view.findViewById(R.id.imgDay2weather);
        imgDay3weather = (ImageView) view.findViewById(R.id.imgDay3weather);
        imgDay4weather = (ImageView) view.findViewById(R.id.imgDay4weather);
        imgDay5weather = (ImageView) view.findViewById(R.id.imgDay5weather);

        thisMonthLayout = (LinearLayout) view.findViewById(R.id.thisMonthLayout);
        thisYearLayout = (LinearLayout) view.findViewById(R.id.thisYearLayout);
        lifeTimeLayout = (LinearLayout) view.findViewById(R.id.lifeTimeLayout);
        systemSizeLayout = (LinearLayout) view.findViewById(R.id.systemSizeLayout);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                "GOTHAM-BOOK.OTF");
        Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(),
                "GOTHAM-MEDIUM.OTF");
        Typeface tf3 = Typeface.createFromAsset(getActivity().getAssets(),
                "LATO-SEMIBOLD.TTF");

        txtDay1Date.setTypeface(tf2);
        txtDay2Date.setTypeface(tf2);
        txtDay3Date.setTypeface(tf2);
        txtDay4Date.setTypeface(tf2);
        txtDay5Date.setTypeface(tf2);

        Day1ProfitCost.setTypeface(tf2);
        Day2ProfitCost.setTypeface(tf2);
        Day3ProfitCost.setTypeface(tf2);
        Day4ProfitCost.setTypeface(tf2);
        Day5ProfitCost.setTypeface(tf2);

        Day1Temp.setTypeface(tf);
        Day2Temp.setTypeface(tf);
        Day3Temp.setTypeface(tf);
        Day4Temp.setTypeface(tf);
        Day5Temp.setTypeface(tf);

        txtThisMonth.setTypeface(tf);
        txtThisYear.setTypeface(tf);
        txtLifeTime.setTypeface(tf);
        txtSystemSize.setTypeface(tf);
        txtBillForeCast.setTypeface(tf);
        txtLastBill.setTypeface(tf);

        thisMonthPower.setTypeface(tf3);
        thisYearPower.setTypeface(tf3);
        lifeTimePower.setTypeface(tf3);
        systemSizePower.setTypeface(tf3);
        foreCastBill.setTypeface(tf3);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.thisMonthLayout:

                Intent intent = new Intent(getActivity(),
                        ProductionChart.class);
                startActivity(intent);

                break;
            case R.id.thisYearLayout:
                Intent intent2 = new Intent(getActivity(),
                        ProductionChart.class);
                startActivity(intent2);
                break;
            case R.id.lifeTimeLayout:
                Intent intent3 = new Intent(getActivity(),
                        ProductionChart.class);
                startActivity(intent3);
                break;
            case R.id.systemSizeLayout:
                Intent intent4 = new Intent(getActivity(),
                        ProductionChart.class);
                startActivity(intent4);
                break;
            default:
                break;
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
}
