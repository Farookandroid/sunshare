package com.sunshare.fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.sunshare.sunshare.R;

import java.util.Calendar;

/**
 * Created by user on 19/7/16.
 */
public class TransferService extends Fragment {

    EditText edt_old_address, edt_loction, edt_new_address, edt_city, edt_zip;
    TextView txt_old_add, txt_new_add, txt_city, txt_zip, txt_location;
    static boolean mIsPremium = true;
    String stringOfDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer_service, container, false);

        intializeUI(view);

        edt_old_address.setKeyListener(null);
        edt_new_address.setKeyListener(null);

        edt_old_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsPremium = true;
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "Date Picker");
            }
        });
        edt_new_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsPremium = false;
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "Date Picker");
            }
        });

        return view;
    }

    private void intializeUI(View view) {

        txt_old_add = (TextView) view.findViewById(R.id.txt_old_add);
        txt_location = (TextView) view.findViewById(R.id.txt_location);
        txt_city = (TextView) view.findViewById(R.id.txt_citytxt);
        txt_zip = (TextView) view.findViewById(R.id.txt_ziptxt);
        txt_new_add = (TextView) view.findViewById(R.id.txt_newaddtxt);

        edt_old_address = (EditText) view.findViewById(R.id.edt_old_address);
        edt_loction = (EditText) view.findViewById(R.id.edt_location);
        edt_new_address = (EditText) view.findViewById(R.id.edt_newaddress);
        edt_city = (EditText) view.findViewById(R.id.edt_city);
        edt_zip = (EditText) view.findViewById(R.id.edt_zip);

    }

    public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            //Use the current date as the default date in the date picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMinDate(c.getTimeInMillis());
            return dialog;


            //Create a new DatePickerDialog instance and return it
        /*
            DatePickerDialog Public Constructors - Here we uses first one
            public DatePickerDialog (Context context, DatePickerDialog.OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth)
            public DatePickerDialog (Context context, int theme, DatePickerDialog.OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth)
         */

        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            //Do something with the date chosen by the user
            // TextView tv = (TextView) getActivity().findViewById(R.id.edt_d);
            if (mIsPremium) {
                stringOfDate = day + "/" + month + "/" + year;
                edt_old_address.setText(stringOfDate);

            } else {
                stringOfDate = day + "/" + month + "/" + year;
                edt_new_address.setText(stringOfDate);

//            wfTime.setText("" + dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            }

        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


}

