package com.sunshare.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.sunshare.sunshare.R;

/**
 * Created by user on 13/7/16.
 */
public class EnvironmentalImpact extends Fragment {

    ViewPager vpPager;
    ViewPagerAdapter viewPagerAdapter;
    SmartTabLayout viewPagerTab;

    String[] impactLines = {"669,225 passenger car miles driven",
            "450 houses build",
            "Planting 4,266 trees",
            "Save 2250 litres water"};
    int[] impactBg = {R.drawable.road, R.drawable.house, R.drawable.tree, R.drawable.water};

    private int currentpos;

    TextView txtEnergy, txtBenefit, txtEnergyUnit, lifetimeEnergy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_environmental_impact, container, false);


        txtEnergy = (TextView) view.findViewById(R.id.txtEnergy);
        txtBenefit = (TextView) view.findViewById(R.id.txtBenefit);
        txtEnergyUnit = (TextView) view.findViewById(R.id.txtEnergyUnit);

        lifetimeEnergy = (TextView) view.findViewById(R.id.energy);


        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                "GOTHAM-BOOK.OTF");
        Typeface tf2 = Typeface.createFromAsset(getActivity().getAssets(),
                "GOTHAM-MEDIUM.OTF");

        txtEnergy.setTypeface(tf);
        lifetimeEnergy.setTypeface(tf);
        txtEnergyUnit.setTypeface(tf);
        txtBenefit.setTypeface(tf2);

        viewPagerTab = (SmartTabLayout) view.findViewById(R.id.viewpagertab);

        vpPager = (ViewPager) view.findViewById(R.id.vpPager);

        viewPagerAdapter = new ViewPagerAdapter(getActivity());

        vpPager.setAdapter(viewPagerAdapter);

        viewPagerTab.setViewPager(vpPager);

        vpPager.setClipToPadding(false);
        vpPager.setPageMargin(30);
        vpPager.setHorizontalFadingEdgeEnabled(false);
        vpPager.setFadingEdgeLength(80);

        return view;
    }


    public class ViewPagerAdapter extends PagerAdapter {

        Context context;
        LayoutInflater inflater;

        ImageView impactImageView;
        TextView impactTextView;

        public ViewPagerAdapter(Context context) {
            this.context = context;
            Log.v("EnviroImpact", "context");
        }

        @Override
        public int getCount() {
            return impactBg.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemView = inflater.inflate(R.layout.fragment_environment_tree,
                    container, false);

            impactImageView = (ImageView) itemView.findViewById(R.id.impact_bg);
            impactTextView = (TextView) itemView.findViewById(R.id.impact_lines);

            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                    "GOTHAM-BOOK.OTF");
            impactTextView.setTypeface(tf);

            impactImageView.setImageResource(impactBg[position]);
            impactTextView.setText(impactLines[position]);

            ((ViewPager) container).addView(itemView);

            return itemView;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((LinearLayout) object);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

}
