package com.sunshare.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunshare.ModelClasses.AddCardModel;
import com.sunshare.sunshare.MainActivity;
import com.sunshare.sunshare.R;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by user on 20/7/16.
 */
public class AddCreditCard extends Fragment implements View.OnClickListener {

    TextView month_picker;
    TextView year_picker;

    TextView txt_card_number, txt_month, txt_year, txt_security_code, txt_zip_code;
    EditText card_number, security_code, zip_code;

    Button linkAccount;

    private Calendar calendar;
    private int year, month, day;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_credit_card, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        intializeUI(view);

        month_picker.setOnClickListener(this);
        year_picker.setOnClickListener(this);
        linkAccount.setOnClickListener(this);

        return view;
    }

    public static Fragment newInstance(int position) {

        AddCreditCard addCreditCard = new AddCreditCard();
        return addCreditCard;
    }

    private void intializeUI(View view) {

        month_picker = (TextView) view.findViewById(R.id.month_picker);
        year_picker = (TextView) view.findViewById(R.id.year_picker);

        txt_card_number = (TextView) view.findViewById(R.id.txt_card_number);
        txt_month = (TextView) view.findViewById(R.id.txt_month);
        txt_year = (TextView) view.findViewById(R.id.txt_year);
        txt_security_code = (TextView) view.findViewById(R.id.txt_security_code);
        txt_zip_code = (TextView) view.findViewById(R.id.txt_zip_code);

        card_number = (EditText) view.findViewById(R.id.card_number);
        security_code = (EditText) view.findViewById(R.id.security_code);
        zip_code = (EditText) view.findViewById(R.id.zip_code);

        linkAccount = (Button) view.findViewById(R.id.link_account);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),
                "GOTHAM-LIGHT.OTF");
        month_picker.setTypeface(tf);
        year_picker.setTypeface(tf);

        txt_card_number.setTypeface(tf);
        txt_month.setTypeface(tf);
        txt_year.setTypeface(tf);
        txt_security_code.setTypeface(tf);
        txt_zip_code.setTypeface(tf);

        card_number.setTypeface(tf);
        security_code.setTypeface(tf);
        zip_code.setTypeface(tf);

        linkAccount.setTypeface(tf);

        card_number.setCursorVisible(false);
        security_code.setCursorVisible(false);
        zip_code.setCursorVisible(false);

        card_number.setOnClickListener(this);
        security_code.setOnClickListener(this);
        zip_code.setOnClickListener(this);

    }


    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
//        String expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
        month_picker.setText("" + month);
        year_picker.setText("" + year);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.card_number:
                card_number.setCursorVisible(true);
                break;

            case R.id.security_code:
                security_code.setCursorVisible(true);
                break;

            case R.id.zip_code:
                zip_code.setCursorVisible(true);
                break;

            case R.id.month_picker:

                card_number.setCursorVisible(true);
                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), myDateListener, year, month, 0);
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                Calendar c = Calendar.getInstance();
                ((ViewGroup) datePickerDialog.getDatePicker()).findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
                c.add(Calendar.DATE, 1);
                Date newDate = c.getTime();
                datePickerDialog.getDatePicker().setMinDate(newDate.getTime());
                datePickerDialog.show();

                break;
            case R.id.year_picker:
                month_picker.performClick();
                break;
            case R.id.link_account:

                AddCardModel model = new AddCardModel();

                model.setCardType("Debit Card");
                model.setCardNumber(card_number.getText().toString());
                model.setCardMonth(month_picker.getText().toString());
                model.setCardYear(year_picker.getText().toString());
                model.setSecurityCode(security_code.getText().toString());
                model.setZipCode(zip_code.getText().toString());

                //  cardModels.add(model);

                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("value", model);
                intent.putExtras(bundle);
                getActivity().setResult(2, intent);
                getActivity().finish();

                break;
            default:
                break;
        }
    }

    private void hideKeyboard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
