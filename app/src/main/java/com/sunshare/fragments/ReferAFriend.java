package com.sunshare.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sunshare.Adapters.ReferAFriendAdapter;
import com.sunshare.sunshare.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by user on 13/7/16.
 */
public class ReferAFriend extends Fragment {

    Button btn_share;
    ViewPager mViewPager;
    int currentPage = 0;
    ReferAFriendAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_refer_a_friend, container, false);

        btn_share = (Button) view.findViewById(R.id.Share_My_Referral);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                // Add data to the intent, the receiving app will decide
                // what to do with it.
                share.putExtra(Intent.EXTRA_SUBJECT, "SunShare");
                share.putExtra(Intent.EXTRA_TEXT, "http://www.codeofaninja.com");

                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
        int[] mResources = {
                R.drawable.ic_first,
                R.drawable.ic_second,
                R.drawable.ic_third
        };
        mAdapter = new ReferAFriendAdapter(getActivity(), mResources);

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setAdapter(mAdapter);

        final Handler mHandler = new Handler();

        // Create runnable for posting

        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (mViewPager.getCurrentItem() == mAdapter.getCount() - 1) {
                    currentPage = 0;
                }
                mViewPager.setCurrentItem(currentPage++, true);
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 2000);

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            //   menu.removeItem(R.id.action_search);
            MenuItem item = menu.findItem(R.id.add_card);
            if (item != null) {
                item.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

}
