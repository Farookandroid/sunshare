package com.sunshare.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunshare.sunshare.R;

public class PdfListAdapter extends BaseAdapter {
    String[] result;
    Context context;
    private static LayoutInflater inflater = null;

    public PdfListAdapter(Context context, String[] prgmNameList) {
        result = prgmNameList;
        this.context = context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView pdfName;
        ImageView pdfImg;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.fragment_doc_listitem, null);
        holder.pdfName = (TextView) rowView.findViewById(R.id.pdf_name);
        holder.pdfImg = (ImageView) rowView.findViewById(R.id.img_pdf);
        holder.pdfName.setText(result[position]);
        holder.pdfImg.setImageResource(R.drawable.document_icon);

        return rowView;
    }

} 